# Moby Monorepo

This repository is the host of the deployment configurations for the **mobylib** project.

There are 3 possible deployment configurations:
- local
- testing
- production

The choice of creating this monorepo was to decouple as much as possible the deployment configurations of
the project of the actual development side.

## What is mobylib?

Mobylib is a web backend project which simulates a library implementing the following features:
- Authentication and authorization using JWT tokens. There are 2 roles, ADMIN and READER, which have different access privileges to the library.
- Adding, listing, removing and updating authors and books in the library.
- Subscriptions to the library.
- Mailing to the subscribed users when a new book is added.

![Simple overview](mobylib.png)

I developed the project while taking the [summer workshop classes](https://ocw.cs.pub.ro/courses/moby) offered by [MOBYLab](https://www.facebook.com/mobylabUPB/) @ [ACS, UPB](https://acs.pub.ro/en/). They provided the necessary knowledge and guidance for me to be able to create this project.

## Local

**Purpose**: local testing on developer's machine.\
**Location**: [deployment/local](deployment/local)\
**Details**: [deployment/local/README.md file](deployment/local/README.md)

## Testing

**Purpose**: global testing in cloud using docker images.\
**Location**: [deployment/testing](deployment/testing)\
**Details**: [deployment/testing/README.md file](deployment/testing/README.md)

## Production

**Purpose**: production environment in cloud.\
**Location**: [deployment/production](deployment/production)\
**Details**: [deployment/production/README.md file](deployment/production/README.md)

## CI/CD
The CI/CD part of the application is a **WIP**. The plan is to use gitlabCI to create 2 pipelines:
- testing environment pipeline (for the testing branches)
- production environment pipeline (for the master branches)

## Before anything else

You should use this repo to also clone the submodules (which are the microservices repos).
First use:\
```git submodule update --init --recursive```\
to clone the submodules.

Then, every time new changes occur in the submodules, use:\
```git submodule update --remote```\
to update submodules to latest commit on master.
