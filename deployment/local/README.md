# Local Deployment

**Purpose**: local testing;\
**Where**: on local machine.

If:
- you want to get a quick grasp of the application
- you don't have access to cloud infrastructure
- you want to test the code you wrote for the application
then this deployment option is appropriate.

## Prerequisites

- docker, docker-compose.
- curl / Postman for testing.
- npm (if you use hot-reload which is the default)

## Before beginning

It may seem odd for the more experienced DevOps to see secrets and other such things in a local environment configuration. However I tried to emulate a production environment as much as possible even if that's not the purpose of this configuration.

Please note that it is **NOT recommended** to expose .env files and docker secrets publicly as I do here but I made sure that I don't leave any sensitive information. If you plan to fork / further develop the project, it is indicated that you add *secrets* and *.env* to your *.gitignore* file.

**VERY IMPORTANT!** Please also note that the way the current configuration is written, it enables **hot-reload** so you can simply save the code that you write and the code inside the container will also update and will force the server to restart so you can leave the cluster running in the background and as soon as you save the file that you are working on, you will be able to test your code. 

This feature makes the life of the developer easier as you don't need to update the containers running in the background every time you make a change to the code.

However, this is enabled by mapping the entire directory of the microservice to the docker container filesystem using volumes. What this means is that you will need to install the npm packages locally and have the node-modules and all other dependencies already built locally so that the container can use them.

If you want to **disable hot-reload** so that it's no longer necessary for you to build the dependencies, please see **Step 4**. 

## Steps

The following steps assume that you have already cloned the repo and the necessary submodules as indicated by the
main README.md in the root of this repo.

### Step 1 (optional)

In the .env file:
- modify MAILADDRESS environment variable with the email address that you want the library to use to send emails to users.
- modify MAILUSER environment variable with the email user corresponding to the MAILADDRESS.

If you don't do this, the mailing feature will not work.

### Step 2 (optional)

In the secrets/mailing_mailpassword_secret.txt file:
- Remove the content of the file and put the password of the MAILADDRESS provided in Step 1.

If you don't do this, the mailing feature will not work.

### Step 3 (optional)

If you want, you can change the rest of the docker secrets in the secrets directory with your own by simply removing the content of the files and adding your own.

### Step 4 (optional)

If you want to disable **hot-reload**, simply remove these lines from the docker-compose.yml file:
-       - ../../moby-library-microservice:/usr/src/app
-       - ../../moby-auth-microservice:/usr/src/app
-       - ../../moby-newsletter-microservice:/usr/src/app
If you remove them, the contents of the /usr/src/app directory inside the containers won't be overridden by the local directories.

If you choose to keep the **hot-reload** feature, please make sure you installed the necessary dependencies with npm in all microservices.

### Step 5

```docker network create moby-workshop-cluster```
This is going to create the main network in the microservies cluster.

## Step 6

```docker-compose up```
This is going to build the images and start the containers. Now you can use the app!
