# Testing Deployment

**Purpose**: global testing;\
**Where**: cloud / on-prem cluster.

If:
- you have access to a cloud infrastructure / an on-prem cluster (at least 2 VMs)
- you want to test the code that ended up on the test branch. (WIP, CI/CD needed)
then this deployment option is appropriate.

## Prerequisites

local machine:
- ssh - to connect to VMs.
- curl / Postman for testing.

on cloud / cluster VMs:
- docker, docker-compose.

## Before beginning

Please note that it is **NOT recommended** to expose .env files and docker secrets publicly as I do here but I made sure that I don't leave any sensitive information. If you plan to fork / further develop the project, it is indicated that you add *secrets* and *.env* to your *.gitignore* file.

Please also note that it is expected to clone the repository on all clusters because I used volumes to bind to certain scripts on the host. It is also mandatory that the path to the project on all VMs is **THE SAME**. 

## Steps

The following steps assume that you have already cloned the repo **(on all VMs where you plan to deploy the docker swarm cluster)** and the necessary submodules as indicated by the main README.md in the root of this repo.

### Step 1 (optional)

In the .env file:
- modify MAILADDRESS environment variable with the email address that you want the library to use to send emails to users.
- modify MAILUSER environment variable with the email user corresponding to the MAILADDRESS.

If you don't do this, the mailing feature will not work.

### Step 2 (optional)

In the secrets/mailing_mailpassword_secret.txt file:
- Remove the content of the file and put the password of the MAILADDRESS provided in Step 1.

If you don't do this, the mailing feature will not work.

### Step 3 (optional)

If you want, you can change the rest of the docker secrets in the secrets directory with your own by simply removing the content of the files and adding your own.

### Step 4

Inject the env variables from the .env file in the SHELL. Unlike the local deployment, when we deploy using swarm, we cannot use the .env file the way we can with the docker-compose command.

To do this, you can use:
```export $(grep -v '^#' .env | xargs)```

### Step 5

On the VM that you plan to use as cluster manager, execute:
```docker swarm init```

On the rest of the VMs, use the exact command provided in the output of the above command to register to the newly
created cluster.
Make sure VMs are in the same network / can reach each other. You can use tools like ping to test that.

### Step 6

```docker network create -d overlay moby-workshop-cluster```
This is going to create the main network in the microservies cluster. It needs to be overlay so that containers in different VMs can communicate with each other.

## Step 7

```docker stack deploy -c docker-compose.yml testing```
This is going to deploy the testing stack in the swarm cluster. Now you can use the app!
The app is going to be listening at **http://<VM_IP>:8002**.

## Step 8 (optional but highly recommended)

Add a Portainer service to the cluster to easily monitor your cluster. For example, you could use:
```docker service create --name portainer --publish 9000:9000 --replicas=1 --constraint 'node.role == manager' --mount type=bind,src=/opt/portainer-data,dst=/data --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock portainer/portainer ```

For more details, please refer to the [portainer documentation](https://www.portainer.io/documentation/).