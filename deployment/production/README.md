## EVEN IF THIS IS VERY CLOSE TO AN ACTUAL PRODUCTION CONFIGURATION, YOU SHOULD NOT BE USING THIS CONFIGURATION FOR PRODUCTION AS THERE ARE SECURITY ISSUES THAT NEED TO BE ADDRESSED.

# Production Deployment

**Purpose**: production;\
**Where**: cloud / on-prem cluster.

If:
- you have access to a cloud infrastructure / an on-prem cluster (at least 3 VMs / 2VMs and 1 NFS server)
- you want to use the code that ended up on the master branch. (WIP, CI/CD needed)
then this deployment option is appropriate.

## Prerequisites

local machine:
- ssh - to connect to VMs.
- curl / Postman / browsers for testing / usage.

on cloud / cluster VMs:
- docker, docker-compose.
- [docker-volume-netshare plugin](https://github.com/ContainX/docker-volume-netshare)

## Before beginning

Please note that it is **NOT recommended** to expose .env files and docker secrets publicly as I do here but I made sure that I don't leave any sensitive information. If you plan to fork / further develop the project, it is indicated that you add *secrets* and *.env* to your *.gitignore* file.

Because I want to create a production environment, I need a NFS (Network File Storage) server so that I can have a layer of abstraction over the underlying filesystem so that the containers won't depend on the hosts where they are deployed.

Please also note that it is expected to clone the repository on all clusters because I used volumes to bind to certain scripts on the host. It is also mandatory that the path to the project on all VMs is **THE SAME**. This should not normally happen in a production environment and the actual scripts should have been put on the NFS server and mounted as volumes the way the Kong configuration file is. This is a WIP.

## Steps

The following steps assume that you have already cloned the repo **(on all VMs where you plan to deploy the docker swarm cluster)** and the necessary submodules as indicated by the main README.md in the root of this repo. It is also assumed that you installed the docker-volume-netshare plugin.

### Step 0

We need a NFS server. You can create your own or build one using docker.

To build one using docker:
1. copy the *nfs_server.yml* file to the VM that you are planning to use as a NFS server.
2. ```mkdir /nfsdata; mkdir /nfsdata/library; mkdir /nfsdata/auth; mkdir /nfsdata/notification; mkdir /nfsdata/kong```
3. copy the kong/kong.yml file to the /nfsdata/kong directory on the NFS server.
4. ```docker-compose -f nfs_server.yml up```

That's it, the NFS server should be up and running.

Then you need to edit the volumes in the docker-compose.yml to use your NFS server.
Change all the lines that look like this:
```      share: 172.105.245.5/something```
to
```      share: <YOUR_NFS_SERVER_IP>/something```.

### Step 1 (optional)

In the .env file:
- modify MAILADDRESS environment variable with the email address that you want the library to use to send emails to users.
- modify MAILUSER environment variable with the email user corresponding to the MAILADDRESS.

If you don't do this, the mailing feature will not work.

### Step 2 (optional)

In the secrets/mailing_mailpassword_secret.txt file:
- Remove the content of the file and put the password of the MAILADDRESS provided in Step 1.

If you don't do this, the mailing feature will not work.

### Step 3 (optional)

If you want, you can change the rest of the docker secrets in the secrets directory with your own by simply removing the content of the files and adding your own.

### Step 4

Inject the env variables from the .env file in the SHELL. Unlike the local deployment, when we deploy using swarm, we cannot use the .env file the way we can with the docker-compose command.

To do this, you can use:
```export $(grep -v '^#' .env | xargs)```

### Step 5

**DO THIS ON ALL VMs used by the swarm cluster**
Start the NFS mode of the plugin:
```docker-volume-netshare nfs```

### Step 5

On the VM that you plan to use as cluster manager, execute:
```docker swarm init```

On the rest of the VMs, use the exact command provided in the output of the above command to register to the newly
created cluster.
Make sure VMs are in the same network / can reach each other. You can use tools like ping to test that.

### Step 6

```docker network create -d overlay moby-workshop-cluster-prod```
This is going to create the main network in the microservies cluster. It needs to be overlay so that containers in different VMs can communicate with each other.

## Step 7

```docker stack deploy -c docker-compose.yml production```
This is going to deploy the production stack in the swarm cluster. Now you can use the app!
The app is going to be listening at **http://<VM_IP>:8000**.

## Step 8 (optional but highly recommended)

Add a Portainer service to the cluster to easily monitor your cluster. For example, you could use:
```docker service create --name portainer --publish 9000:9000 --replicas=1 --constraint 'node.role == manager' --mount type=bind,src=/opt/portainer-data,dst=/data --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock portainer/portainer ```

For more details, please refer to the [portainer documentation](https://www.portainer.io/documentation/).
